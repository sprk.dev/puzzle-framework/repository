"use strict";

const PObject = require("@puzzleframework/core/Core/PObject");
const DB = require("@puzzleframework/db/DB");


/**
 * Class CriteriaBase
 */
class CriteriaBase extends PObject {
  /**
   * Operators object. To be used when constructing the where string
   * and you need operators for various comparisons.
   *
   * See {@link http://docs.sequelizejs.com/manual/tutorial/querying.html#operators}
   *
   * @public
   * @property {Sequelize.Op}
   */
  Op = DB.Sequelize.Op;

  /**
   * Reference to Sequelize object.
   *
   * @public
   * @property {Sequelize}
   */
  Sequelize = DB.Sequelize;

  /**
   * A transaction object if you want the data to be used in a transaction.
   *
   * @protected
   * @property {object}
   */
  _transaction = null;

  get transaction() {
    return this._transaction;
  }

  set transaction(newValue) {
    this._transaction = newValue;
  }

  /**
   * A list with models that have to be included.
   *
   * @protected
   * @property {Array}
   */
  _include = [];

  get include() {
    return this._include;
  }

  set include(newValue) {
    this._include = newValue;
    this._distinct = this._include.length !== 0;
  }

  /**
   * A list with fields on which the query will be grouped by.
   *
   * @protected
   * @property {Array}
   */
  _group = [];

  get group() {
    return this._group;
  }

  set group(newValue) {
    this._group = newValue;
  }

  /**
   * A dictionary with attributes specific to the query, like
   * which fields to be included or excluded etc.
   *
   * @protected
   * @property {object}
   */
  _attributes = {};

  get attributes() {
    return this._attributes;
  }

  set attributes(newValue) {
    this._attributes = newValue;
  }

  /**
   * A dictionary with conditions used to filter the data in
   * the database.
   *
   * @protected
   * @property {object}
   */
  _where = {};

  get where() {
    return this._where;
  }

  set where(newValue) {
    this._where = newValue;
  }

  /**
   * A list with fields on which you want the data to be ordered by.
   *
   * @protected
   * @property {Array}
   */
  _order = [];

  get order() {
    return this._order;
  }

  set order(newValue) {
    this._order = newValue;
  }

  /**
   * Should the limit be included in the subqueries or not?
   *
   * @protected
   * @property {Boolean}
   */
  _subQuery = false;

  /**
   * Should the limit be included in the subqueries or not?
   *
   * @return {Boolean}
   */
  get subQuery() {
    return this._subQuery;
  }

  /**
   * Should the limit count only the distinct elements?
   *
   * @protected
   * @property {Boolean}
   */
  _distinct = true;

  /**
   * Should the limit count only the distinct elements?
   *
   * @return {Boolean}
   */
  get distinct() {
    return this._distinct;
  }

  enableSubQuery() {
    this._subQuery = true;
  }

  enableDistinct() {
    this._distinct = true;
  }

  disableSubQuery() {
    this._subQuery = false;
  }

  disableDistinct() {
    this._distinct = false;
  }

  /**
   * Returns the object/array used to build the Criteria
   * for usage with sequelize.
   *
   * @return {object}
   */
  buildWhereObject() {
    return this._where;
  }

  /**
   * Returns the object/array used to build the Criteria
   * for usage with sequelize.
   *
   * @return {object}
   */
  buildAttributesObject() {
    return this._attributes;
  }

  /**
   * Returns the object/array used to build the Criteria
   * for usage with sequelize.
   *
   * @return {Array}
   */
  buildIncludeObject() {
    return this._include;
  }

  /**
   * Returns the object/array used to build the Criteria
   * for usage with sequelize.
   *
   * @return {Array}
   */
  buildOrderObject() {
    return this._order;
  }

  /**
   * Returns the object/array used to build the Criteria
   * for usage with sequelize.
   *
   * @return {Array}
   */
  buildGroupObject() {
    return this._group;
  }

  /**
   * Returns the object/array used to build the Criteria
   * for usage with sequelize transactions.
   *
   * @return {object}
   */
  buildTransactionObject() {
    return this._transaction;
  }
}

module.exports = CriteriaBase;
