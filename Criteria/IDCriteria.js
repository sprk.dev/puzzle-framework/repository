"use strict";

const CriteriaBase = require("./CriteriaBase");

/**
 * Class IDCriteria
 */
class IDCriteria extends CriteriaBase {
  /**
   * Constructor of the ID filter Criteria
   *
   * @param {string} id The ID on which we filter.
   * @param {Object|null} [transaction=null] The transaction object if you want
   *                                         the data to be used in a transaction.
   */
  constructor(id, transaction = null) {
    super();

    this.transaction = transaction;

    this.where = {
      id
    };
  }
}

module.exports = IDCriteria;
