"use strict";

const PError = require("@puzzleframework/core/Exceptions/HTTPError");

/**
 * Class ElementNotFoundException
 */
class ElementNotFoundException extends PError {
  /**
   * Constructor of the repository exception.
   */
  constructor() {
    super("The requested element was not found.");
  }
}

module.exports = ElementNotFoundException;
