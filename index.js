"use strict";

const PRuntime = require("@puzzleframework/core/Core/PRuntime");
const puzzle = require("@puzzleframework/core/Puzzle");
const joi = require("@hapi/joi");

const RepositoryLoader = require("./RepositoryLoader");

/**
 * Repository related module
 *
 * @namespace @puzzleframework.repository
 */

/**
 * Initialization class for the beast.base module.
 *
 * @memberOf @puzzleframework.repository
 */
class PuzzleRepository extends PRuntime {
  constructor() {
    super();

    puzzle.set("repository", new RepositoryLoader());
    puzzle.set("Joi", joi);
  }
}

module.exports = PuzzleRepository;
